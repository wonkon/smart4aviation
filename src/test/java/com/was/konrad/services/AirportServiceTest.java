package com.was.konrad.services;

import org.junit.Assert;
import org.junit.Test;

public class AirportServiceTest {

    private AirportService airportService = new AirportService();

    @Test
    public void when_get_flight_number_and_departure_date_then_return_number_of_flight_departing() {
        //given
        String iataCode = "KRK";
        String Date = "2017-10-07";
        //when
        final int result = airportService.numberOfFlightDeparting(iataCode, Date);
        //then
        Assert.assertEquals(1, result);
    }

    @Test
    public void when_get_flight_number_and_departure_date_then_return_number_of_flight_arriving() {
        //given
        String iataCode = "KRK";
        String Date = "2017-10-07";
        //when
        final int result = airportService.numberOfFlightArriving(iataCode, Date);
        //then
        Assert.assertEquals(1, result);
    }

    @Test
    public void when_get_flight_number_and_departure_date_then_return_pieces_of_baggage_departing() {
        //given
        String iataCode = "KRK";
        String Date = "2017-10-07";
        //when
        final int result = airportService.piecesOfBaggageDepartingFromThisAirport(iataCode, Date);
        //then
        Assert.assertEquals(4437, result);
    }

    @Test
    public void when_get_flight_number_and_departure_date_then_return_pieces_of_baggage_arriving() {
        //given
        String iataCode = "KRK";
        String Date = "2017-10-07";
        //when
        final int result = airportService.numberOfBaggageArrivingToThisAirport(iataCode, Date);
        //then
        Assert.assertEquals(3042, result);
    }
}