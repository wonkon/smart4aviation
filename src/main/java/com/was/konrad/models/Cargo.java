package com.was.konrad.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Cargo {
    private Integer id;
    private Integer weight;
    private String weightUnit;
    private Integer pieces;
}